import express, { Request, Response} from "express";
import mongoose from "mongoose";

import dotenv from "dotenv";
import cors from "cors";
import session, {Session} from "express-session";
import { join } from "path";
import { log } from "console";

export const app = express ();

dotenv.config();


//view engine setup
app.use(express.json());
app.use(cors());

app.use(express.static(join(__dirname, "../public")));
app.set("views",join(__dirname, "views"));
app.set("view engine","ejs");

app.use(
    session({
        secret:process.env.SESSION_SECRET as string,
        resave:false,
        saveUninitialized:false,
    })
);

//conectar a la base de datos 
const options: mongoose.ConnectOptions = {
    dbName:process.env.DB_NAME as string,
    user:process.env.DB_USER as string,
    pass:process.env.DB_PASS as string,

};

(async()=>{
    await mongoose.connect(process.env.DB_CONNECTION as string,options);
    console.log("conectado a mongo db");
})();

app.get('/',(req:Request, res: Response)=>{
    res.send("hi");
});

app.listen(3001,()=>{
    console.log("server running on 3001");
})